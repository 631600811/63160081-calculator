import 'dart:io';

class calculator {
  String input = '';
  List<String> num = [];
  List<String> operator = [];

  void showwelcome() {
    print('Please input:');
    input = stdin.readLineSync()!;
    calculate(input);
  }

  bool checknum(String s) {
    if (s != '+' && s != '-' && s != '*' && s != '/') {
      return true;
    }
    return false;
  }

  calculate(String input) {
    String temp = '';
    for (int i = 0; i < input.length; i++) {
      if (checknum(input[i])) {
        temp = temp + input[i];
      } else {
        operator.add(input[i]);
        num.add(temp);
        temp = '';
      }
    }
    num.add(temp);
    int result = int.parse(num[0]);
    num.removeAt(0);

    while (operator.length != 0) {
      if (operator[0] == '+') {
        result = result + int.parse(num[0]);
        operator.removeAt(0);
        num.removeAt(0);
      } else if (operator[0] == '-') {
        result = result - int.parse(num[0]);
        operator.removeAt(0);
        num.removeAt(0);
      } else if (operator[0] == '*') {
        result = result * int.parse(num[0]);
        operator.removeAt(0);
        num.removeAt(0);
      } else if (operator[0] == '/') {
        result = result ~/ int.parse(num[0]);
        operator.removeAt(0);
        num.removeAt(0);
      }
    }
    print(result.toString());
  }
}
